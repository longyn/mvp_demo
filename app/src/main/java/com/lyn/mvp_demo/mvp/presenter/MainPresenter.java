package com.lyn.mvp_demo.mvp.presenter;

import com.lyn.mvp_demo.mvp.contract.MainContract;
import com.lyn.mvp_demo.mvp.model.MainModel;
import com.lyn.mvp_demo.mvp.model.Result;

/**
 * @author:longyongning
 * @date:2022/3/23
 * @description:
 */
public class MainPresenter implements MainContract.Presenter {

    private MainModel mainModel;
    private MainContract.View view;

    public MainPresenter(MainContract.View view) {
        mainModel = new MainModel();
        this.view = view;
    }


    @Override
    public void login(String userName, String password) {
        MainAsyncTask<String> mainAsyncTask = new MainAsyncTask(view, (MainContract.AsyncTask<String>) () ->
                mainModel.login(userName, password));
        mainAsyncTask.execute();

    }
}
