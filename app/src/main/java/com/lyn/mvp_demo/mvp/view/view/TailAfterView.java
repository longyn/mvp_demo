package com.lyn.mvp_demo.mvp.view.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewTreeObserver;

import androidx.annotation.Nullable;

public class TailAfterView extends View implements ViewTreeObserver.OnGlobalLayoutListener {

    private Paint paint;
    private RectF rectF;
    private float widths;
    private float heights;


    public synchronized void setRectF(RectF rectF) {
        this.rectF = rectF;
        postInvalidate();
    }

    public float getWidths() {
        return widths;
    }

    public float getHeights() {
        return heights;
    }

    public TailAfterView(Context context) {
        super(context);
        initialize();
    }

    public TailAfterView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    private void initialize() {
        paint = new Paint();
        paint.setColor(Color.YELLOW);
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(3);
        paint.setTextSize(40f);
        paint.setTextAlign(Paint.Align.CENTER);
        getViewTreeObserver().addOnGlobalLayoutListener(this);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        canvas.drawRect(0,0,widths,heights,paint);
        if (rectF == null) {
            return;
        }
        canvas.drawRect(rectF, paint);
    }

    private float baseLine(float y) {
        //计算baseline
        Paint.FontMetrics fontMetrics = paint.getFontMetrics();
        float distance = (fontMetrics.bottom - fontMetrics.top) / 2 - fontMetrics.bottom;
        float baseline = y + distance;
        return baseline;
    }

    @Override
    public void onGlobalLayout() {
        this.widths = getWidth();
        this.heights = getHeight();
    }
}
