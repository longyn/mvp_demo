package com.lyn.mvp_demo.mvp.model;

/**
 * @author:longyongning
 * @date:2022/3/24
 * @description:
 */
public class Result <T>{

    public final static int SUCCESS = 1;
    public final static int FAIL = 0;

    private int code;

    private String msg;

    private T t;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }
}
