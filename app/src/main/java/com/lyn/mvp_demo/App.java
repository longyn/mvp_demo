package com.lyn.mvp_demo;

import android.app.Application;
import android.content.Context;

import com.lyn.mmkvPlus.MMKVPlus;

import org.litepal.LitePal;

public class App extends Application {

    public static Context context;

    public static void setContext(Context context) {
        App.context = context;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        setContext(this);
        LitePal.initialize(context);
        MMKVPlus.getInstance().initialize(context);
    }
}
