package com.lyn.mvp_demo.mvp.view.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.viewbinding.ViewBinding;

import com.lyn.mvp_demo.R;


/**
 * @author:longyongning
 * @date:2022/3/23
 * @description:
 */
public abstract class BaseActivity<V extends ViewBinding> extends AppCompatActivity {

    public V viewBinding;
    public Context context;
    public ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = getViewBinding();
        setContentView(viewBinding.getRoot());
        context = this;
        create(savedInstanceState);
    }


    public abstract V getViewBinding();

    public abstract void create(@Nullable Bundle savedInstanceState);


    public void toast(String msg) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show();
    }

    public void showLoading() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(context);
            progressDialog.setTitle(getString(R.string.prompt));
            progressDialog.setMessage(getString(R.string.pleaseWait));
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
        }
        progressDialog.show();

    }

    public void hideLoading() {
        if (progressDialog == null) return;
        progressDialog.dismiss();
    }

    public void jump(Class<?> aClass) {
        Intent intent = new Intent(context, aClass);
        startActivity(intent);
    }

    public void jump(Class<?> aClass, boolean isFinishing) {
        Intent intent = new Intent(context, aClass);
        startActivity(intent);
        if (isFinishing) {
            finish();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (isFinishing()) {
            if (progressDialog != null) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }
}
