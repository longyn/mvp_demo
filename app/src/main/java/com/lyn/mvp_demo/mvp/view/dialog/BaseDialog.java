package com.lyn.mvp_demo.mvp.view.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.viewbinding.ViewBinding;

import com.lyn.mvp_demo.R;

public abstract class BaseDialog<V extends ViewBinding> extends Dialog {

    public V viewBinding;

    public BaseDialog(@NonNull Context context) {
        super(context, R.style.common_dialog);
    }

    public BaseDialog(@NonNull Context context, int themeResId) {
        super(context, themeResId);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewBinding = getViewBinding();
        setContentView(viewBinding.getRoot());
        create(this);
    }

    public abstract V getViewBinding();

    public abstract void create(BaseDialog<V> dialog);
}
