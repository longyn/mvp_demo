package com.lyn.mvp_demo.service.andServer.contract;

import com.lyn.mvp_demo.service.andServer.entity.RequestResult;

public interface AndContract {

    interface Controller{
        RequestResult login(String userName,String password);
    }

    interface Service{
        RequestResult login(String userName,String password);
    }
}
