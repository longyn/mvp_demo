package com.lyn.mvp_demo.mvp.presenter;

import android.os.AsyncTask;
import com.lyn.mvp_demo.mvp.contract.MainContract;
import com.lyn.mvp_demo.mvp.model.Result;

/**
 * @author:longyongning
 * @date:2022/3/24
 * @description:
 */
public class MainAsyncTask<T> extends AsyncTask<Void,Integer,Result<T>> {

    private MainContract.View<T> view;
    private MainContract.AsyncTask asyncTask;

    public MainAsyncTask(MainContract.View<T> view, MainContract.AsyncTask<T> asyncTask) {
        this.view = view;
        this.asyncTask = asyncTask;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        view.showLoading();
    }

    @Override
    protected Result<T> doInBackground(Void... voids) {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return asyncTask.doInBackground();
    }

    @Override
    protected void onProgressUpdate(Integer... values) {
        super.onProgressUpdate(values);

    }

    @Override
    protected void onPostExecute(Result result) {
        super.onPostExecute(result);
        view.hideLoading();
        if (result.getCode()==Result.SUCCESS){
            view.onSuccess(result);
        }else {
            view.onFail("请求服务器失败!");
        }
    }

}
