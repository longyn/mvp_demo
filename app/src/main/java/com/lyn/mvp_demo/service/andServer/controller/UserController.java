package com.lyn.mvp_demo.service.andServer.controller;

import com.lyn.mvp_demo.service.andServer.contract.AndContract;
import com.lyn.mvp_demo.service.andServer.entity.RequestResult;
import com.lyn.mvp_demo.service.andServer.service.UserService;
import com.yanzhenjie.andserver.annotation.PostMapping;
import com.yanzhenjie.andserver.annotation.RequestMapping;
import com.yanzhenjie.andserver.annotation.RequestParam;
import com.yanzhenjie.andserver.annotation.RestController;

@RestController
@RequestMapping(value = "/user")
public class UserController implements AndContract.Controller {

    private final UserService userService = new UserService();

    @PostMapping(value = "/login")
    public RequestResult login(@RequestParam("userName") String userName, @RequestParam("password") String password) {
        return userService.login(userName, password);
    }
}
