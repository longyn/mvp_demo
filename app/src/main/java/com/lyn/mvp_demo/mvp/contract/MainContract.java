package com.lyn.mvp_demo.mvp.contract;


import com.lyn.mvp_demo.mvp.model.Result;

/**
 * @author:longyongning
 * @date:2022/3/23
 * @description:
 */
public interface MainContract {

    interface Model <T>{
        Result<T> login(String userName, String password);
    }

    interface View <T>{
        void showLoading();

        void hideLoading();

        void onSuccess(Result<T> result);

        void onFail(String s);
    }

    interface Presenter {
        void login(String userName, String password);
    }

    interface AsyncTask <T>{
        Result<T> doInBackground();
    }
}
