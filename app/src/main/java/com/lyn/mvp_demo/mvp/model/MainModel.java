package com.lyn.mvp_demo.mvp.model;

import com.lyn.mvp_demo.mvp.contract.MainContract;


/**
 * @author:longyongning
 * @date:2022/3/23
 * @description:
 */
public class MainModel implements MainContract.Model<String> {


    @Override
    public Result<String> login(String userName, String password) {
        Result result = new Result();
        result.setCode(Result.SUCCESS);
        result.setMsg("登录成功");
        return null;
    }
}
