package com.lyn.mvp_demo.mvp.view.activity;

import android.hardware.Camera;
import android.os.Bundle;
import com.face.SeetaFaceHelper;
import com.lyn.mvp_demo.databinding.ActivityMainBinding;
import com.lyn.mvp_demo.mvp.contract.MainContract;
import com.lyn.mvp_demo.mvp.model.Result;
import com.lyn.mvp_demo.mvp.presenter.MainPresenter;
import com.lyn.mvp_demo.mvp.view.fragment.CameraFragment;

public class MainActivity extends BaseActivity<ActivityMainBinding> implements MainContract.View<String>, Camera.PreviewCallback {

    private MainPresenter presenter;
    private CameraFragment cameraFragment;

    @Override
    public ActivityMainBinding getViewBinding() {
        return ActivityMainBinding.inflate(getLayoutInflater());
    }

    @Override
    public void create(Bundle savedInstanceState) {
        try {
            SeetaFaceHelper.getInstance().init(context);
        } catch (Exception e) {
            e.printStackTrace();
        }
        presenter = new MainPresenter(this);
        cameraFragment = new CameraFragment();
        cameraFragment.setPreviewCallback(this);
        getSupportFragmentManager().beginTransaction()
                .replace(viewBinding.cameraFrameLayout.getId(), cameraFragment)
                .commitNow();

    }


    @Override
    public void onSuccess(Result<String> result) {

    }

    @Override
    public void onFail(String s) {

    }


    @Override
    public void onPreviewFrame(byte[] bytes, Camera camera) {

    }





}